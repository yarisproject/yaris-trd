yarisApp.controller('interiorController', function($scope) {
    var sliderSelection = 0;
    var currentShowPopup = -1;
    $scope.interiorInit = function(){
        $('.page-interior').css({'opacity':0.01});
        $('.page-interior .page-content').css({'opacity':0.01});
        jQuery('.page-interior').imagesLoaded()
            .always( function( instance ) {
            console.log('all images loaded');
        })
            .done( function( instance ) {
            console.log('all images successfully loaded');
            $scope.imageLoaded();
        })
            .fail( function() {
            console.log('all images loaded, at least one is broken');
        })
            .progress( function( instance, image ) {
            var result = image.isLoaded ? 'loaded' : 'broken';
            console.log( 'image is ' + result + ' for ' + image.img.src );
        });
        /*
        jQuery(document).ready(function() {
            jQuery(".owl-carousel").owlCarousel(
                {
                    items : 4,
                    loop : true,
                    nav : true,
                    navText : '',
                }
            );
            $('.link').on('click', function(event){
                var $this = $(this);
                var contentIndex = $this.data('content-index');
                showContentIndex(contentIndex);
            });

        });
*/        
    }
    $scope.imageLoaded = function(){
        console.log('imageLoaded');
        //window.setTimeout(setDefaultHeight, 100);
        //setDefaultHeight();
        //$('.popup-group').fadeOut(0);
        window.setTimeout(function(){
            setDefaultHeight();
            $('.popup-group').fadeOut(0);
        }, 100);
        
        var scale = getScreenScale();
        $('body,.page-interior').css({
            height: 1050*scale
        });
    }

    $scope.thumbClick = function(inx){
        var scale = getScreenScale();
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var footerHeight = $('.foot-nav').height();
        var navHeight = ($('nav').height()-32)*scale;
        console.log('inx', inx);
        TweenMax.to('.foot-nav li>a.active', 0.05, {marginRight:0, marginTop:0, paddingLeft:0, paddingBottom:0});
        $('.foot-nav li>a.active').removeClass('active');
        $('.foot-nav li>a:eq('+inx+')').addClass('active');
        TweenMax.to('.foot-nav li>a.active', 0.05, {marginRight:(-6*scale), marginTop:(-10*scale), paddingLeft:(6*scale), paddingBottom:(10*scale)});
        TweenMax.to('.popup-text', 0.2, {left:-300, opacity:0});
        if(currentShowPopup == -1){
            $('.popup-group').fadeIn();
        }
        $('.popup').each(function(index){
            if(index != inx){
                $('.popup:eq('+index+')').fadeOut(); 
            }else{
                $('.popup:eq('+index+')').fadeIn();
                TweenMax.to('.popup-text:eq('+index+')', 0.3, {left:0, opacity:1,delay:0.5});
                var maxCarHeight = $('.page-interior').height() - footerHeight - navHeight;
                var carHeight = $('.page-interior .popup-car:eq('+index+')').height();
                var carWidth = $('.page-interior .popup-car:eq('+index+')').width();
                var carPosTop = '0px';
                carWidth = carWidth * (maxCarHeight/carHeight);
                carPosTop = navHeight+'px';
                $('.page-interior .popup-car:eq('+index+')').css({
                    'width' : carWidth+'px',
                    'top'   : carPosTop,
                    'margin-left': -carWidth/2+'px',
                    'left'  : '50%'
                });
            }
        });

    }

    $scope.removePopup = function(){
        $('.popup-group').fadeOut();
        TweenMax.to('.foot-nav li>a.active', 0.05, {marginRight:0, marginTop:0, paddingLeft:0, paddingBottom:0});
        $('.foot-nav li>a.active').removeClass('active');
        currentShowPopup = -1;
    }

    function setDefaultHeight(){
        var scale = getScreenScale();
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        $('.page-interior .check-scale').each(function(){
            var width = $(this).width();
            $(this).width(width*scale);
        });
        TweenMax.to('.page-interior', 1, {opacity:1,delay:0.1});
        TweenMax.to('.page-interior .page-content', 0.5, {opacity:1,delay:0.1});


        var footerHeight = $('.page-interior .foot-nav>ul>li img').height() + 40*scale;
        $('.page-interior .foot-nav').css({
            'height': '0px',
        });
        TweenMax.to('.foot-nav', 0.5, {height:footerHeight,delay:1});

        $('.page-interior .lbl-interior').css({
            'top': (footerHeight - $('.page-interior .lbl-interior').height())/2+'px',
            'right' : '100%'
        });
        TweenMax.to('.lbl-interior', 0.3, {right:'60%',delay:1.5});

        $('.page-interior .foot-nav ul').css({
            'margin-top': (20*scale)+'px',
            'opacity'   : 0,
            'padding-left': 0
        });
        TweenMax.to('.foot-nav ul', 0.2, {opacity:1, paddingLeft:(40*scale),delay:1.8});

        $('.page-interior .foot-nav ul li').css({
            'margin-left': -(40*scale)+'px',
        });
/*
        $('.page-interrior .model').css({
            'left':180*scale+'px'    
        });

        $('.page-interrior .text').css({
            'right':126*scale+'px' ,
            'bottom': (22*scale) + $('.page-interrior foot-nav').height()+'px',
        });

        $('.page-interrior .popup-text').css({
            'top':150*scale+'px',    
        });
*/
        $('.page-interior .text').css({
            'left':'-20%',
            'bottom': '22%',
            'opacity': 0
        });
        TweenMax.to('.page-interior .text', 0.3, {left: 0, 'opacity':1,delay:1.5});

    }

    function getScreenScale(){
        var pageHeight = $(window).height();
        var pageWidth = $(window).width();
        console.log((pageWidth / 1680));
        widthScale = (pageWidth / 1680);
        heightScale = (pageHeight / 1050);
        if(widthScale > heightScale){
            return heightScale;
        }else{
            return widthScale;
        }
    }
});