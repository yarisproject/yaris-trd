var yarisApp = angular.module('yarisApp', ['ngRoute', 'ngAnimate']);

// configure our routes
yarisApp.config(function($routeProvider, $locationProvider) {
    $routeProvider.when('/home', {
        templateUrl : 'pages/home.html?1',
        controller  : 'homeController'
    }).when('/exterior', {
        templateUrl : 'pages/exterior.html?1',
        controller  : 'exteriorController'
    }).when('/interior', {
        templateUrl : 'pages/interior.html?1',
        controller  : 'interiorController'
    }).otherwise({
        controller: "landingController",
        template: "<div></div>"
    });
});