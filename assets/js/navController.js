yarisApp.controller('navController', function($scope,$location) {
    var defNavWidth = 1404;
    var defNavHeight = 138;
    $scope.navInit = function(){
        var scale = getScreenScale();
        var windowWidth = $(window).width();
        var headHeight = $('header').height();
        var headWidth = $('header').width();
        var newHeadHeight = $('header').height() * scale;
        var newHeadWidth = $('header').width() * scale;
        var newNavWidth = defNavWidth * scale;
        var newNavHeight = defNavHeight * scale;
        var newTopHead = -(headHeight - newHeadHeight)/2;
        var navGoLeft = (windowWidth - newNavWidth)/2;
        $('header').css({
            'left'              : navGoLeft+'px',
            '-webkit-transform' : 'scale(' + scale + ')',
            '-moz-transform'    : 'scale(' + scale + ')',
            '-ms-transform'     : 'scale(' + scale + ')',
            '-o-transform'      : 'scale(' + scale + ')',
            'transform'         : 'scale(' + scale + ')'
        });
        var navHash = window.location.hash;
        if(navHash == '#/exterior'){
            setNavPosition('51.3%', 0.01);
            $('.nav-exterior').addClass('active');
        }else if(navHash == '#/interior'){
            setNavPosition('64.5%', 0.01);
            $('.nav-interior').addClass('active');
        }else{
            $('.nav-home').addClass('active');
        }
        $('nav>ul>li>a').on('click', function(){
            var arrPosition = ['38.2%','38.2%','38.2%','51.3%','64.5%'];
            var $index = $('nav>ul>li>a').index(this);
            $('nav>ul>li>a').removeClass('active');
            $(this).addClass('active');
            setNavPosition(arrPosition[$index], 0.3);
        });
    }
    function setNavPosition(position, time){
        TweenMax.to('.activeNav', time, {left:position, ease: Sine.easeInOut,delay:0.001});
    }

    function getScreenScale(){
        var pageHeight = $(window).height();
        var pageWidth = $(window).width();
        widthScale = (pageWidth / 1680).toFixed(6);
        heightScale = (pageHeight / 1050).toFixed(6);
        if(widthScale > heightScale){
            return heightScale;
        }else{
            return widthScale;
        }
    }
    /*
    $scope.navigateUpdate = function(){
        if(jQuery('nav a.exterior').hasClass('hover')){
            console.log('exterior');
            jQuery('header').css('background-position', '50% -250px');
        }else if(jQuery('nav a.interior').hasClass('hover')){
            console.log('interior');
            jQuery('header').css('background-position', '50% -500px');
        }else if(jQuery('nav a.utility').hasClass('hover')){
            console.log('utility');
            jQuery('header').css('background-position', '50% -750px');
        }else if(jQuery('nav a.performance').hasClass('hover')){
            console.log('performance');
            jQuery('header').css('background-position', '50% -1000px');
        }else if(jQuery('nav a.safety').hasClass('hover')){
            console.log('safety');
            jQuery('header').css('background-position', '50% -1250px');
        }else if(jQuery('nav a.accessories').hasClass('hover')){
            console.log('accessories');
            jQuery('header').css('background-position', '50% -1500px');
        }else if(jQuery('nav a.lineup').hasClass('hover')){
            console.log('lineup');
            jQuery('header').css('background-position', '50% -1750px');
        }else if(jQuery('nav a.specification').hasClass('hover')){
            console.log('specification');
            jQuery('header').css('background-position', '50% -2000px');
        }else if(jQuery('nav a.home').hasClass('hover')){
            console.log('home');
            jQuery('header').css('background-position', '50% 0');
        }else{
            jQuery('header').css('background-position', '50% 0px');
            $scope.checkUpdate();
        }
    }
    $scope.checkUpdate = function(){
        if(jQuery('nav a.exterior').hasClass('active')){
            console.log('exterior');
            jQuery('header').css('background-position', '50% -250px');
        }else if(jQuery('nav a.interior').hasClass('active')){
            console.log('interior');
            jQuery('header').css('background-position', '50% -500px');
        }else if(jQuery('nav a.utility').hasClass('active')){
            console.log('utility');
            jQuery('header').css('background-position', '50% -750px');
        }else if(jQuery('nav a.performance').hasClass('active')){
            console.log('performance');
            jQuery('header').css('background-position', '50% -1000px');
        }else if(jQuery('nav a.safety').hasClass('active')){
            console.log('safety');
            jQuery('header').css('background-position', '50% -1250px');
        }else if(jQuery('nav a.accessories').hasClass('active')){
            console.log('accessories');
            jQuery('header').css('background-position', '50% -1500px');
        }else if(jQuery('nav a.lineup').hasClass('active')){
            console.log('lineup');
            jQuery('header').css('background-position', '50% -1750px');
        }else if(jQuery('nav a.specification').hasClass('active')){
            console.log('specification');
            jQuery('header').css('background-position', '50% -2000px');
        }else if(jQuery('nav a.home').hasClass('active')){
            console.log('home');
            jQuery('header').css('background-position', '50% 0px');
        }else{
            jQuery('header').css('background-position', '50% 0px');
        }
    } 
    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
    */
});