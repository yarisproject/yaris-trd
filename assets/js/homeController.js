var lightInterval = 0;
yarisApp.controller('homeController', function($scope) {
    var lightImageURLs = [];
    var lightImagesOK = 0;
    var lightImgs = [];
    var lightPosX = 0;
    var moveX = 28;
    var maskImg, lightImg;
    $scope.homeInit = function(){
        $('.page-home').css({'opacity':0.01});
        $('.page-home .page-content').css({'opacity':0.01});
        lightImageURLs.push("assets/imgs/home/mask-mirror.png");
        lightImageURLs.push("assets/imgs/home/mask-light.png");
        loadLightImages();
        console.log('init home');
        jQuery('.page-home').imagesLoaded()
            .always( function( instance ) {
            console.log('all images loaded');
        })
            .done( function( instance ) {
            console.log('all images successfully loaded');
            $scope.imageLoaded();
        })
            .fail( function() {
            console.log('all images loaded, at least one is broken');
        })
            .progress( function( instance, image ) {
            var result = image.isLoaded ? 'loaded' : 'broken';
            console.log( 'image is ' + result + ' for ' + image.img.src );
        });
    }

    $scope.imageLoaded = function(){
        window.setTimeout(setDefaultHeight, 100);
        console.log('imageLoaded');
        var scale = getScreenScale();
        var screenHeight = 1050*scale;
        $('body,.page-home').css({
            height: screenHeight
        });
    }
    function loadLightImages(){
        for (var i = 0; i < lightImageURLs.length; i++) {
            var img = new Image();
            lightImgs.push(img);
            img.onload = function () {
                lightImagesOK++;
                imagesLightLoaded();
            };
            img.src = lightImageURLs[i];
        }
    }
    function imagesLightLoaded(){
        if (lightImagesOK == lightImageURLs.length) {
            maskImg = lightImgs[0];
            lightImg = lightImgs[1];
        }
    }
    function setDefaultHeight(){
        var scale = getScreenScale();
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        $('.page-home .check-scale').each(function(){
            var width = $(this).width();
            $(this).width(width*scale);
        });
        TweenMax.to('.page-home', 1, {opacity:1,delay:0.1});
        TweenMax.to('.page-home .page-content', 0.5, {opacity:1,delay:0.1});
        //var modelPosTop = windowHeight - $('.page-home .model').height() + (370*scale);
        var modelPosBottom = -$('.page-home .model').height() * 0.284;
        $('.page-home .model').css({
            'left': -(68*scale)+'px',
            'bottom' : modelPosBottom - (370*scale) + 'px',
            'opacity' : 0.01
        });
        TweenMax.to('.model', 0.5, {opacity:1, bottom:modelPosBottom,delay:1.6});
        $('.page-home .prop').css({
            'left': '30%',
            'bottom' : '0',
            'opacity' : 0.01
        });
        TweenMax.to('.prop', 0.5, {opacity:1,delay:1.6});
        //$('.page-home .car').height($('.page-home .car').height()*2);
        var carPosLeft = ($('.page-home').width() - $('.page-home .car').width() + (160*scale) )/2;
        var carPosTop = ($('.page-home').height() - $('.page-home .car').height() - (20*scale) )/2;

        $('.page-home .car').css({
            'left': carPosLeft - (500 * scale)+'px',
            'top' : carPosTop - (110 * scale)+'px',
            'opacity' : 0.01
        });
        TweenMax.to('.car', 0.5, {opacity:1, left:carPosLeft, top:carPosTop,delay:2.2});
        $('#light-mirror').css({
            'width' : $('.page-home .car').width()+'px',
            'top' : carPosTop+'px',
            'left' : carPosLeft+'px',
            'opacity' :0
        });
        TweenMax.to('#light-mirror', 0.5, {opacity:1, delay:2.2});
        if(lightInterval != 0){
            clearInterval(lightInterval);
        }
        lightInterval = setInterval(drawLight, 30);

        var newYarisPos = ((windowWidth - $('.page-home .new-yaris').width() - (214*scale) ) / 2);
        
        $('.page-home .new-yaris').css({
            'left': (newYarisPos * 2)+'px',
            'top' : (150*scale)+'px',
            'opacity' : 0.01
        });
        TweenMax.to('.new-yaris', 0, {opacity:0.01, left:1000});
        TweenMax.to('.new-yaris', 0.3, {opacity:1, left:newYarisPos,delay:2.7});
        $('.page-home .one-that-right').css({
            'left': ((windowWidth - $('.page-home .one-that-right').width() + (180*scale)) / 2) +'px',
            'bottom' : '5%',
            'opacity': 0.01
        });
        TweenMax.to('.one-that-right', 0.5, {opacity:1,delay:3});
        var youtubeWidth = 560*scale;
        var youtubeHeight = 315*scale;
        $('.page-home .youtube').css({
            'position':'absolute',
            'left': ($('.page-home .one-that-right').position().left + $('.page-home .one-that-right').width() + (10*scale)) +'px',
            'bottom': '5%',
            'width' : youtubeWidth+'px',
            'height': youtubeHeight+'px',
            'opacity': 0.01
        });
        TweenMax.to('.youtube', 0.5, {opacity:1,delay:3});
    }
    function drawLight(){
        var canvas = document.getElementById("light-mirror");
        if(canvas !== null && lightImagesOK == 2){
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0,0,canvas.width, canvas.height);

            if(lightPosX > canvas.width+1000 || lightPosX<-600) moveX=-moveX;
            lightPosX += moveX;
            ctx.save();
            ctx.drawImage(maskImg, 0, 0);
            ctx.globalCompositeOperation = "source-in";
            ctx.drawImage(lightImg, lightPosX, -60);
            ctx.restore();
        }
    }

    function getScreenScale(){
        var pageHeight = $(window).height();
        var pageWidth = $(window).width();
        console.log((pageWidth / 1680));
        widthScale = (pageWidth / 1680);
        heightScale = (pageHeight / 1050);
        if(widthScale > heightScale){
            return heightScale;
        }else{
            return widthScale;
        }
    }
});