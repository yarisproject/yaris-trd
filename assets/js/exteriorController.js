var lightIntervalExterior = 0;
yarisApp.controller('exteriorController', function($scope) {
    var sliderSelection = 0;
    var currentShowPopup = -1;
    var lightImageURLs = [];
    var lightImagesOK = 0;
    var lightImgs = [];
    var lightPosX = 0;
    var moveX = 28;
    var maskImg, lightImg;
    $scope.exteriorInit = function(){
        $('.page-exterior').css({'opacity':0.01});
        $('.page-exterior .page-content').css({'opacity':0.01});
        lightImageURLs.push("assets/imgs/exterior/open-scene/mask-mirror.png");
        lightImageURLs.push("assets/imgs/home/mask-light.png");
        loadLightImages();
        jQuery('.page-exterior').imagesLoaded()
            .always( function( instance ) {
            console.log('all images loaded');
        })
            .done( function( instance ) {
            console.log('all images successfully loaded');
            $scope.imageLoaded();
        })
            .fail( function() {
            console.log('all images loaded, at least one is broken');
        })
            .progress( function( instance, image ) {
            var result = image.isLoaded ? 'loaded' : 'broken';
            console.log( 'image is ' + result + ' for ' + image.img.src );
        });

        /*
        jQuery(document).ready(function() {
            jQuery(".owl-carousel").owlCarousel(
                {
                    items : 4,
                    loop : true,
                    nav : true,
                    navText : '',
                }
            );
            $('.link').on('click', function(event){
                var $this = $(this);
                var contentIndex = $this.data('content-index');
                showContentIndex(contentIndex);
            });

        });
        */
    }
    function loadLightImages(){
        for (var i = 0; i < lightImageURLs.length; i++) {
            var img = new Image();
            lightImgs.push(img);
            img.onload = function () {
                lightImagesOK++;
                imagesLightLoaded();
            };
            img.src = lightImageURLs[i];
        }
    }
    function imagesLightLoaded(){
        if (lightImagesOK == lightImageURLs.length) {
            maskImg = lightImgs[0];
            lightImg = lightImgs[1];
        }
    }

    $scope.imageLoaded = function(){
        //setDefaultHeight();
        window.setTimeout(function(){
            setDefaultHeight();
            $('.popup-group').fadeOut(0);
        }, 100);
        console.log('imageLoaded exterior');
        
        var scale = getScreenScale();
        $('body,.page-exterior').css({
            height: 1050*scale
        });
    }

    $scope.thumbClick = function(inx){
        var scale = getScreenScale();
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        var footerHeight = $('.foot-nav').height();
        var navHeight = ($('nav').height()-32)*scale;
        console.log('inx', inx);
        TweenMax.to('.foot-nav li>a.active', 0.05, {marginRight:0, marginTop:0, paddingLeft:0, paddingBottom:0});
        $('.foot-nav li>a.active').removeClass('active');
        $('.foot-nav li>a:eq('+inx+')').addClass('active');
        TweenMax.to('.foot-nav li>a.active', 0.05, {marginRight:(-6*scale), marginTop:(-10*scale), paddingLeft:(6*scale), paddingBottom:(10*scale)});
        TweenMax.to('.popup-text', 0.2, {left:(-300*scale), opacity:0});
        if(currentShowPopup == -1){
            $('.popup-group').fadeIn();
        }
        $('.popup').each(function(index){
            if(index != inx){
                $('.popup:eq('+index+')').fadeOut();
            }else{
                $('.popup:eq('+index+')').fadeIn();
                TweenMax.to('.popup-text:eq('+index+')', 0.3, {left:0, opacity:1,delay:0.5});
                var maxCarHeight = $('.page-exterior').height() - footerHeight - navHeight;
                var carHeight = $('.page-exterior .popup-car:eq('+index+')').height();
                var carWidth = $('.page-exterior .popup-car:eq('+index+')').width();
                var carPosTop = '0px';
                carWidth = carWidth * (maxCarHeight/carHeight);
                carPosTop = navHeight+'px';
                $('.page-exterior .popup-car:eq('+index+')').css({
                    'width' : carWidth+'px',
                    'top'   : carPosTop,
                    'margin-left': -carWidth/2+'px',
                    'left'  : '50%'
                });
            }
        });

    }

    $scope.removePopup = function(){
        $('.popup-group').fadeOut();
        TweenMax.to('.foot-nav li>a.active', 0.05, {marginRight:0, marginTop:0, paddingLeft:0, paddingBottom:0});
        $('.foot-nav li>a.active').removeClass('active');
        currentShowPopup = -1;
    }

    function setDefaultHeight(){
        var scale = getScreenScale();
        var windowWidth = $(window).width();
        var windowHeight = $(window).height();
        $('.page-exterior .check-scale').each(function(){
            var width = $(this).width();
            $(this).width(width*scale);
        });
        TweenMax.to('.page-exterior', 1, {opacity:1,delay:0.1});
        TweenMax.to('.page-exterior .page-content', 0.5, {opacity:1,delay:0.1});
        var carPosLeft = '38%';
        var carPosTop = ($('.page-exterior').height() - $('.page-exterior .car').height() + 60*scale ) / 2;
        $('.page-exterior .car').css({
            'left'  : '100%',
            'top'   :  0,
            'opacity'   : 0
        });
        TweenMax.to('.car', 0.5, {opacity:1, left:carPosLeft, top:carPosTop,delay:0.5});

        $('#light-mirror-exterior').css({
            'width' : $('.page-exterior .car').width()+'px',
            'top' : carPosTop+'px',
            'left' : carPosLeft,
            'opacity' :0
        });
        TweenMax.to('#light-mirror-exterior', 0.5, {opacity:1, delay:1});
        if(lightIntervalExterior != 0){
            clearInterval(lightIntervalExterior);
        }
        lightIntervalExterior = setInterval(drawLightExterior, 30);

        $('.page-exterior .model').css({
            'right' : '60%',
            'bottom': '-1000px'
        });
        TweenMax.to('.model', 0.5, {opacity:1, bottom:$('.page-exterior .model').height() *0.11,delay:0.5});

        var footerHeight = $('.page-exterior .foot-nav>ul>li img').height() + 40*scale;
        $('.page-exterior .foot-nav').css({
            'height': '0px',
        });
        TweenMax.to('.foot-nav', 0.5, {height:footerHeight,delay:0.5});

        $('.page-exterior .lbl-exterior').css({
            'top': (footerHeight - $('.page-exterior .lbl-exterior').height())/2+'px',
            'right' : '100%'
        });
        TweenMax.to('.lbl-exterior', 0.3, {right:'70%',delay:1.5});

        $('.page-exterior .foot-nav ul').css({
            'margin-top': (20*scale)+'px',
            'opacity'   : 0,
            'padding-left': 0
        });
        TweenMax.to('.foot-nav ul', 0.2, {opacity:1, paddingLeft:(40*scale),delay:1.8});

        $('.page-exterior .foot-nav ul li').css({
            'margin-left': -(40*scale)+'px',
        });

        $('.page-exterior .text').css({
            'right':'-20%',
            'bottom': '22%',
            'opacity': 0
        });
        TweenMax.to('.page-exterior .text', 0.3, {right: 0, 'opacity':1,delay:1.5});

        $('.page-exterior .popup-text').css({
            'top':150*scale+'px',    
        });
    }

    function drawLightExterior(){
        var canvas = document.getElementById("light-mirror-exterior");
        if(canvas !== null && lightImagesOK == 2){
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0,0,canvas.width, canvas.height);

            if(lightPosX > canvas.width+1000 || lightPosX<-600) moveX=-moveX;
            lightPosX += moveX;
            ctx.save();
            ctx.drawImage(maskImg, 0, 0);
            ctx.globalCompositeOperation = "source-in";
            ctx.drawImage(lightImg, lightPosX, -60);
            ctx.restore();
        }
    }
    function getScreenScale(){
        var pageHeight = $(window).height();
        var pageWidth = $(window).width();
        widthScale = (pageWidth / 1680);
        heightScale = (pageHeight / 1050);
        if(widthScale > heightScale){
            return heightScale;
        }else{
            return widthScale;
        }
    }
});